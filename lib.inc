%define ten 10
%define ascii_nul 0x0
%define ascii_tab 0x9
%define ascii_newline 0xA
%define ascii_space 0x20


section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    push rdi
    xor rcx, rcx

    .loop:
        cmp byte [rdi], 0
        jz .return
        inc rcx
        inc rdi
    jmp .loop

    .return:
        pop rdi
        mov rax, rcx
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    push rcx
    call string_length
    pop rcx
    pop rdi
    mov rdx, rax
    push rsi
    push rdi
    
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall

    pop rdi
    pop rsi
    xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rsi
    push rdi

    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1

    syscall

    pop rdi
    pop rsi

    xor rax, rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ascii_newline
    call print_char
    xor rax, rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rdi
    push rsi
    push rdx
    push rcx

    xor rcx, rcx 

    .dividing_loop:
        xor rdx, rdx
        push rsi
        mov rax, rdi
        mov rsi, ten
        div rsi
        pop rsi
        add rdx, '0'
        push rdx
        mov rdi, rax
        inc rcx
        test rdi, rdi
    jnz .dividing_loop

    .printing_loop:
        pop rdi
        push rcx
        call print_char
        pop rcx
        dec rcx
        test rcx, rcx
    jg .printing_loop

    pop rcx
    pop rdx
    pop rsi
    pop rdi
    xor rax, rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rdi
    test rdi, rdi
    jge .print_num

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi

    neg rdi

    .print_num:
        call print_uint
        pop rdi
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .checking_loop:
        mov r9b, byte[rdi+rcx]
        cmp r9b, byte[rsi+rcx]
        jne .not_equals
        inc rcx
        test r9b, r9b
    jnz .checking_loop
    .equals:
        xor rax, rax
        inc rax
        ret
    .not_equals:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    .reading_loop:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi

        cmp rax, ascii_tab
        jz .check_if_not_empty
        cmp rax, ascii_newline
        jz .check_if_not_empty
        cmp rax, ascii_space
        jz .check_if_not_empty

        cmp rax, ascii_nul
        jz .success

        mov [rdi+rcx], rax
        inc rcx
        cmp rcx, rsi
        jge .too_long
    jmp .reading_loop
    .check_if_not_empty:
        test rcx, rcx
        jz .reading_loop
    .success:
        mov rdx, rcx
        mov byte [rdi+rdx], ascii_nul
        mov rax, rdi
        ret
    .too_long:
        xor rax, rax
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor r8, r8
    mov r9, ten
    xor rax, rax

    .parsing_loop:
        mov r8b, byte [rdi+rcx]
        cmp r8b, '0'
        jl .return
        cmp r8b, '9'
        jg .return
        inc rcx
        sub r8, '0'
        mul r9
        add rax, r8
    jmp .parsing_loop
    .return:
        mov rdx, rcx
        ret
    ;todo




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jnz parse_uint
    inc rdi

    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    call string_length

    cmp rax, rdx
    jg .too_long
    test rdx, rdx
    jz .too_long

    xor rax, rax
    push rdi
    push rsi
    .loop:
        inc rax
        push rcx

        mov cl, byte[rdi]
        mov byte[rsi], cl

        pop rcx

        cmp byte[rdi], 0
        jz .success

        inc rdi
        inc rsi
    jnz .loop
    .success:
        pop rsi
        pop rdi
        ret
    .too_long:
        xor rax, rax
        ret